/*
 * mqtt.h
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Source code strongly inspired by these excellent Apache 2.0 licensed references: */
/* https://docs.zephyrproject.org/latest/reference/networking/mqtt.html */
/* https://docs.zephyrproject.org/latest/samples/net/mqtt_publisher/README.html */

#ifndef MQTT_H
#define MQTT_H

enum mqtt_qos;

struct mqtt_client;
struct mqtt_evt;

void mqtt_broker_init(void);
void mqtt_client_start(struct mqtt_client *client);
void mqtt_event_handler(struct mqtt_client *const client, const struct mqtt_evt *evt);
int mqtt_kapua_birthcertificate(void);
int mqtt_kapua_connect(void);
int mqtt_kapua_disconnect(void);
int mqtt_kapua_publish(uint32_t);
int mqtt_process_mqtt_and_sleep(struct mqtt_client *client, int timeout);
int mqtt_publish_transmissions(uint32_t);
int mqtt_publish_uptime(void);
void mqtt_thread(void);
int mqtt_try_to_connect(struct mqtt_client *client);
int mqtt_wait(int);

#endif
