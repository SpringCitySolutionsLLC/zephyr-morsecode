/*
 * settings.c
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021-2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>

#include <zephyr/settings/settings.h>

#define LOG_LEVEL LOG_LEVEL_DBG
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(morsecode_settings);

#include "main.h"
#include "settings.h"

/* This struct is from the sample for settings module. */
struct direct_immediate_value {
	size_t len;
	void *dest;
	uint8_t fetched;
};

/* This function is from the sample for settings module. */
static int direct_loader_immediate_value(const char *name, size_t len,
					 settings_read_cb read_cb, void *cb_arg,
					 void *param)
{
	const char *next;
	size_t name_len;
	int rc;
	struct direct_immediate_value *one_value =
					(struct direct_immediate_value *)param;

	name_len = settings_name_next(name, &next);

	if (name_len == 0) {
		if (len == one_value->len) {
			rc = read_cb(cb_arg, one_value->dest, len);
			if (rc >= 0) {
				one_value->fetched = 1;
				return 0;
			}

            LOG_ERR("Error reading setting [%d]", rc);
			return rc;
		}
		return -EINVAL;
	}

	/* other keys aren't served by the calback
	 * Return success in order to skip them
	 * and keep storage processing.
	 */
	return 0;
}

/* This function is from the sample for settings module. */
int load_immediate_value(const char *name, void *dest, size_t len)
{
	int rc;
	struct direct_immediate_value dov;

	dov.fetched = 0;
	dov.len = len;
	dov.dest = dest;

	rc = settings_load_subtree_direct(name, direct_loader_immediate_value, (void *)&dov);
	if (rc == 0) {
		if (!dov.fetched) {
			rc = -ENOENT;
		}
	}

	return rc;
}

void settings_boot(void)
{
    int rc;

    rc = settings_subsys_init();
	if (rc) {
        LOG_ERR("Error initializing settings system [%d]", rc);
		return;
	}

	rc = load_immediate_value("hostname", &hostname, sizeof(hostname));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading hostname setting");
        strncpy(hostname, "default_hostname", HOSTNAME_MAX);
		settings_save_hostname();
	}

	rc = load_immediate_value("kapua_appname", &kapua_appname, sizeof(kapua_appname));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading kapua_appname setting");
        strncpy(kapua_appname, "default_appname", KAPUA_APPNAME_MAX);
		settings_save_kapua_appname();
	}

	rc = load_immediate_value("kapua_displayname", &kapua_displayname, sizeof(kapua_displayname));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading kapua_displayname setting");
        strncpy(kapua_displayname, "default_displayname", KAPUA_DISPLAYNAME_MAX);
		settings_save_kapua_displayname();
	}

	rc = load_immediate_value("kapua_tenant", &kapua_tenant, sizeof(kapua_tenant));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading kapua_tenant setting");
        strncpy(kapua_tenant, "default_tenant", KAPUA_TENANT_MAX);
		settings_save_kapua_tenant();
	}

	rc = load_immediate_value("mac_1", &mac_1, sizeof(mac_1));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mac_1 setting");
        strncpy(mac_1, "02", MAC_1_MAX);
		settings_save_mac_1();
	}

	rc = load_immediate_value("mac_2", &mac_2, sizeof(mac_2));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mac_2 setting");
        strncpy(mac_2, "80", MAC_2_MAX);
		settings_save_mac_2();
	}

	rc = load_immediate_value("mac_3", &mac_3, sizeof(mac_3));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mac_3 setting");
        strncpy(mac_3, "E1", MAC_3_MAX);
		settings_save_mac_3();
	}

	rc = load_immediate_value("mac_4", &mac_4, sizeof(mac_4));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mac_4 setting");
		memcpy(mac_4, &device_id_string[18], 2);
		settings_save_mac_4();
	}

	rc = load_immediate_value("mac_5", &mac_5, sizeof(mac_5));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mac_5 setting");
		memcpy(mac_5, &device_id_string[20], 2);
		settings_save_mac_5();
	}

	rc = load_immediate_value("mac_6", &mac_6, sizeof(mac_6));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mac_6 setting");
		memcpy(mac_6, &device_id_string[22], 2);
		settings_save_mac_6();
	}

	rc = load_immediate_value("mqtt_address", &mqtt_address, sizeof(mqtt_address));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mqtt_address setting");
        strncpy(mqtt_address, "0.0.0.0", MQTT_ADDRESS_MAX);
		settings_save_mqtt_address();
	}

	rc = load_immediate_value("mqtt_clientid", &mqtt_clientid, sizeof(mqtt_clientid));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mqtt_clientid setting");
        strncpy(mqtt_clientid, "default_clientid", MQTT_CLIENTID_MAX);
		settings_save_mqtt_clientid();
	}

	rc = load_immediate_value("mqtt_pword", &mqtt_pword, sizeof(mqtt_pword));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mqtt_pword setting");
        strncpy(mqtt_pword, "default_pword", MQTT_PWORD_MAX);
		settings_save_mqtt_pword();
	}

	rc = load_immediate_value("mqtt_uname", &mqtt_uname, sizeof(mqtt_uname));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading mqtt_uname setting");
        strncpy(mqtt_uname, "default_uname", MQTT_UNAME_MAX);
		settings_save_mqtt_uname();
	}

	rc = load_immediate_value("wifi_ssid", &wifi_ssid, sizeof(wifi_ssid));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading wifi_ssid setting");
        strncpy(wifi_ssid, "default_ssid", WIFI_SSID_MAX);
		settings_save_wifi_ssid();
	}

	rc = load_immediate_value("wifi_password", &wifi_password, sizeof(wifi_password));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading wifi_password setting");
        strncpy(wifi_password, "default_password", WIFI_PASSWORD_MAX);
		settings_save_wifi_password();
	}

	rc = load_immediate_value("wpm", &wpm, sizeof(wpm));
	if (rc == -ENOENT) {
        LOG_ERR("Error reading wpm setting");
        strncpy(wpm, "20", WPM_MAX);
		settings_save_wpm();
	}

}

void settings_save_hostname(void)
{
    int rc;

    rc = settings_save_one("hostname", (const void *)&hostname, sizeof(hostname));
	if (rc) {
		LOG_ERR("Error saving hostname [%d]", rc);
	}
}

void settings_save_kapua_appname(void)
{
    int rc;

    rc = settings_save_one("kapua_appname", (const void *)&kapua_appname, sizeof(kapua_appname));
	if (rc) {
		LOG_ERR("Error saving kapua_appname [%d]", rc);
	}
}

void settings_save_kapua_displayname(void)
{
    int rc;

    rc = settings_save_one("kapua_displayname", (const void *)&kapua_displayname, sizeof(kapua_displayname));
	if (rc) {
		LOG_ERR("Error saving kapua_displayname [%d]", rc);
	}
}

void settings_save_kapua_tenant(void)
{
    int rc;

    rc = settings_save_one("kapua_tenant", (const void *)&kapua_tenant, sizeof(kapua_tenant));
	if (rc) {
		LOG_ERR("Error saving kapua_tenant [%d]", rc);
	}
}

void settings_save_mac_1(void)
{
    int rc;

    rc = settings_save_one("mac_1", (const void *)&mac_1, sizeof(mac_1));
	if (rc) {
		LOG_ERR("Error saving mac_1 [%d]", rc);
	}
}

void settings_save_mac_2(void)
{
    int rc;

    rc = settings_save_one("mac_2", (const void *)&mac_2, sizeof(mac_2));
	if (rc) {
		LOG_ERR("Error saving mac_2 [%d]", rc);
	}
}

void settings_save_mac_3(void)
{
    int rc;

    rc = settings_save_one("mac_3", (const void *)&mac_3, sizeof(mac_3));
	if (rc) {
		LOG_ERR("Error saving mac_3 [%d]", rc);
	}
}

void settings_save_mac_4(void)
{
    int rc;

    rc = settings_save_one("mac_4", (const void *)&mac_4, sizeof(mac_4));
	if (rc) {
		LOG_ERR("Error saving mac_4 [%d]", rc);
	}
}

void settings_save_mac_5(void)
{
    int rc;

    rc = settings_save_one("mac_5", (const void *)&mac_5, sizeof(mac_5));
	if (rc) {
		LOG_ERR("Error saving mac_5 [%d]", rc);
	}
}

void settings_save_mac_6(void)
{
    int rc;

    rc = settings_save_one("mac_6", (const void *)&mac_6, sizeof(mac_6));
	if (rc) {
		LOG_ERR("Error saving mac_6 [%d]", rc);
	}
}

void settings_save_mqtt_address(void)
{
    int rc;

    rc = settings_save_one("mqtt_address", (const void *)&mqtt_address, sizeof(mqtt_address));
	if (rc) {
		LOG_ERR("Error saving mqtt_address [%d]", rc);
	}
}

void settings_save_mqtt_clientid(void)
{
    int rc;

    rc = settings_save_one("mqtt_clientid", (const void *)&mqtt_clientid, sizeof(mqtt_clientid));
	if (rc) {
		LOG_ERR("Error saving mqtt_clientid [%d]", rc);
	}
}

void settings_save_mqtt_pword(void)
{
    int rc;

    rc = settings_save_one("mqtt_pword", (const void *)&mqtt_pword, sizeof(mqtt_pword));
	if (rc) {
		LOG_ERR("Error saving mqtt_pword [%d]", rc);
	}
}

void settings_save_mqtt_uname(void)
{
    int rc;

    rc = settings_save_one("mqtt_uname", (const void *)&mqtt_uname, sizeof(mqtt_uname));
	if (rc) {
		LOG_ERR("Error saving mqtt_uname [%d]", rc);
	}
}

void settings_save_wifi_ssid(void)
{
    int rc;

    rc = settings_save_one("wifi_ssid", (const void *)&wifi_ssid, sizeof(wifi_ssid));
	if (rc) {
		LOG_ERR("Error saving wifi_ssid [%d]", rc);
	}
}

void settings_save_wifi_password(void)
{
    int rc;

    rc = settings_save_one("wifi_password", (const void *)&wifi_password, sizeof(wifi_password));
	if (rc) {
		LOG_ERR("Error saving wifi_password [%d]", rc);
	}
}

void settings_save_wpm(void)
{
    int rc;

    rc = settings_save_one("wpm", (const void *)&wpm, sizeof(wpm));
	if (rc) {
		LOG_ERR("Error saving wpm [%d]", rc);
	}
}
