/*
 * morse.h
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021-2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MORSE_H
#define MORSE_H

void morse_thread(void);
void morse_send_message(void);

void on(void);
void off(void);

void dit(void);
void dash(void);

void dit_time(void);
void letter_time(void);
void word_time(void);

void letter_a(void);
void letter_b(void);
void letter_c(void);
void letter_d(void);
void letter_e(void);
void letter_f(void);
void letter_g(void);
void letter_h(void);
void letter_i(void);
void letter_j(void);
void letter_k(void);
void letter_l(void);
void letter_m(void);
void letter_n(void);
void letter_o(void);
void letter_p(void);
void letter_q(void);
void letter_r(void);
void letter_s(void);
void letter_t(void);
void letter_u(void);
void letter_v(void);
void letter_w(void);
void letter_x(void);
void letter_y(void);
void letter_z(void);

void number_1(void);
void number_2(void);
void number_3(void);
void number_4(void);
void number_5(void);
void number_6(void);
void number_7(void);
void number_8(void);
void number_9(void);
void number_0(void);

#endif
