/*
 * mqtt.c
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Source code strongly inspired by these excellent Apache 2.0 licensed references: */
/* https://docs.zephyrproject.org/latest/reference/networking/mqtt.html */
/* https://docs.zephyrproject.org/latest/samples/net/mqtt_publisher/README.html */

#include <zephyr/kernel.h>
#include <zephyr/net/socket.h>
#include <zephyr/net/mqtt.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <zephyr/random/rand32.h>

#define LOG_LEVEL LOG_LEVEL_INF
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(morsecode_mqtt);

/* NANOPB stuff */
#include <pb_encode.h>
#include <pb_decode.h>
/* TODO: Fix CMakeLists.txt instead of working around CMakeLists.txt */
/* The nanopb example works but my app is not including the build directory */
/* and cmake is too complicated to troubleshoot so after a couple hours I */
/* simply hard coded the path and called it good. */
/* This is GitLab issue #10 https://gitlab.com/SpringCitySolutionsLLC/zephyr-morsecode/-/issues/10 */
/* #include "/home/vince/zephyrproject/morsecode/build/src/kurapayload.pb.h" */

#include "../build/src/kurapayload.pb.h"

#include "main.h"
#include "mqtt.h"

#define RX_BUFFER_MAX 128
static uint8_t rx_buffer[RX_BUFFER_MAX];
#define TX_BUFFER_MAX 128
static uint8_t tx_buffer[TX_BUFFER_MAX];

static struct mqtt_client client_ctx;
static struct sockaddr_storage broker;

static struct zsock_pollfd fds[1];
static int nfds;

static bool connected;

void mqtt_broker_init(void)
{
    struct sockaddr_in *broker4 = (struct sockaddr_in *)&broker;

    broker4->sin_family = AF_INET;
    broker4->sin_port = htons(1883);
    zsock_inet_pton(AF_INET, mqtt_address, &broker4->sin_addr);
}

/* mqtt_client_init would seem a very rational name for this function */
/* However, the mqtt subsystem already has a function by that name LOL */
void mqtt_client_start(struct mqtt_client *client)
{
    mqtt_client_init(client);

    mqtt_broker_init();

    struct mqtt_utf8 *mqtt_uname_mqtt_utf8;
    mqtt_uname_mqtt_utf8 = malloc(sizeof(struct mqtt_utf8));
    mqtt_uname_mqtt_utf8->utf8 = mqtt_uname;
    mqtt_uname_mqtt_utf8->size = strlen(mqtt_uname);

    struct mqtt_utf8 *mqtt_pword_mqtt_utf8;
    mqtt_pword_mqtt_utf8 = malloc(sizeof(struct mqtt_utf8));
    mqtt_pword_mqtt_utf8->utf8 = mqtt_pword;
    mqtt_pword_mqtt_utf8->size = strlen(mqtt_pword);

    client->broker = &broker;
    client->evt_cb = mqtt_event_handler;
    client->client_id.utf8 = mqtt_clientid;
    client->client_id.size = strlen(mqtt_clientid);
    client->user_name = mqtt_uname_mqtt_utf8;
    client->password = mqtt_pword_mqtt_utf8;
    client->protocol_version = MQTT_VERSION_3_1_1;

    client->rx_buf = rx_buffer;
    client->rx_buf_size = sizeof(rx_buffer);
    client->tx_buf = tx_buffer;
    client->tx_buf_size = sizeof(tx_buffer);

    client->transport.type = MQTT_TRANSPORT_NON_SECURE;
	fds[0].events = ZSOCK_POLLIN;
	nfds = 1;
}

void mqtt_event_handler(struct mqtt_client *const client, const struct mqtt_evt *evt)
{
    int err;

    switch (evt->type)
    {
    case MQTT_EVT_CONNACK:
        if (evt->result != 0)
        {
            LOG_ERR("mqtt_event_handler: MQTT_EVT_CONNACK: connect failed: [%d]", evt->result);
            break;
        }

        connected = true;
        LOG_DBG("mqtt_event_handler: MQTT_EVT_CONNACK: connect successful");

        break;

    case MQTT_EVT_DISCONNECT:
        LOG_DBG("mqtt_event_handler: MQTT_EVT_DISCONNECT: [%d]", evt->result);

        connected = false;
        nfds = 0;

        break;

    case MQTT_EVT_PUBACK:
        if (evt->result != 0)
        {
            LOG_ERR("mqtt_event_handler: MQTT_EVT_PUBACK: error [%d]", evt->result);
            break;
        }

        LOG_DBG("mqtt_event_handler: MQTT_EVT_PUBACK: packet id: [%u]", evt->param.puback.message_id);

        break;

    case MQTT_EVT_PUBREC:
        if (evt->result != 0)
        {
            LOG_ERR("mqtt_event_handler: MQTT_EVT_PUBREC: error [%d]", evt->result);
            break;
        }

        LOG_DBG("mqtt_event_handler: MQTT_EVT_PUBREC: packet id: [%u]", evt->param.pubrec.message_id);

        const struct mqtt_pubrel_param rel_param = {
            .message_id = evt->param.pubrec.message_id};

        err = mqtt_publish_qos2_release(client, &rel_param);
        if (err != 0)
        {
            LOG_ERR("mqtt_event_handler: MQTT_EVT_PUBREC: failed to send PUBREL: [%d]", err);
        }

        break;

    case MQTT_EVT_PUBCOMP:
        if (evt->result != 0)
        {
            LOG_ERR("mqtt_event_handler: MQTT_EVT_PUBCOMP: error [%d]", evt->result);
            break;
        }

        LOG_DBG("mqtt_event_handler: MQTT_EVT_PUBCOMP: packet id [%u]", evt->param.pubcomp.message_id);

        break;

    case MQTT_EVT_PINGRESP:
        LOG_DBG("mqtt_event_handler: MQTT_EVT_PINGRESP");
        break;

    default:
        break;
    }
}

int mqtt_kapua_birthcertificate(void)
{
    int rc;

    /* If the MQTT server is not configured, do not try to talk to it. */
    while (!strcmp(mqtt_address, "0.0.0.0")) {
        return 0;
    }

    if (connected)
    {
        /* Do NOT forget to free the malloc */
        #define KAPUA_BIRTHCERTIFICATE_TOPIC_MAX 128
        uint8_t *topic = malloc(KAPUA_BIRTHCERTIFICATE_TOPIC_MAX);
        snprintf(topic, KAPUA_BIRTHCERTIFICATE_TOPIC_MAX, "$EDC/%s/%s/MQTT/BIRTH", kapua_tenant, hostname);
        /* A very typical result might be "$EDC/scs/f767zi/MQTT/BIRTH" */
        LOG_DBG("kapua_birthcertificate: Constructed MQTT topic name of: [%s]", topic);

        /* Do NOT forget to free the malloc */
        #define KAPUA_BIRTHCERTIFICATE_PAYLOAD_MAX 1400
        uint8_t *payload = malloc(KAPUA_BIRTHCERTIFICATE_PAYLOAD_MAX);
        memset(payload, 0, KAPUA_BIRTHCERTIFICATE_PAYLOAD_MAX); /* Otherwise testing dumps of the buffer will contain confusing old hawkBit stuff */
        size_t payload_length; /* Is a binary buffer maybe containing 0x00, not a null terminated string */
        kuradatatypes_KuraPayload message = kuradatatypes_KuraPayload_init_default;
        pb_ostream_t stream = pb_ostream_from_buffer(payload, KAPUA_BIRTHCERTIFICATE_PAYLOAD_MAX);

        /* I would at some point like to implement the following applications */

        /* TODO: Implement CMD-V1 ideally running shell commands remotely over MQTT */
        /* This is GitLab wishlist issue #7 https://gitlab.com/SpringCitySolutionsLLC/zephyr-morsecode/-/issues/7 */
        
        /* TODO: Implement ASSET-V1 to remotely control pins and things */
	    /* This is GitLab wishlist issue #8 https://gitlab.com/SpringCitySolutionsLLC/zephyr-morsecode/-/issues/8 */

        strncpy(message.metric[0].name, "application_ids", sizeof(message.metric[0].name));
        strncpy(message.metric[0].string_value, "", sizeof(message.metric[0].string_value));
        message.metric[0].has_string_value = true;
        message.metric[0].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[1].name, "os", sizeof(message.metric[1].name));
        strncpy(message.metric[1].string_value, "Zephyr RTOS", sizeof(message.metric[1].string_value));
        message.metric[1].has_string_value = true;
        message.metric[1].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[2].name, "application_framework", sizeof(message.metric[2].name));
        strncpy(message.metric[2].string_value, "Kura", sizeof(message.metric[2].string_value));
        message.metric[2].has_string_value = true;
        message.metric[2].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[3].name, "os_arch", sizeof(message.metric[3].name));
        strncpy(message.metric[3].string_value, "STM32", sizeof(message.metric[3].string_value));
        message.metric[3].has_string_value = true;
        message.metric[3].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[4].name, "display_name", sizeof(message.metric[4].name));
        strncpy(message.metric[4].string_value, hostname, sizeof(message.metric[4].string_value));
        message.metric[4].has_string_value = true;
        message.metric[4].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[5].name, "display_name", sizeof(message.metric[5].name));
        strncpy(message.metric[5].string_value, hostname, sizeof(message.metric[5].string_value));
        message.metric[5].has_string_value = true;
        message.metric[5].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[6].name, "firmware_version", sizeof(message.metric[6].name));
        strncpy(message.metric[6].string_value, image_build_time, sizeof(message.metric[6].string_value));
        message.metric[6].has_string_value = true;
        message.metric[6].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        /* This is displayed in the "IoT Framework Version" column in "Devices" in Kapua */
        strncpy(message.metric[7].name, "application_framework_version", sizeof(message.metric[7].name));
        strncpy(message.metric[7].string_value, image_build_time, sizeof(message.metric[7].string_value));
        message.metric[7].has_string_value = true;
        message.metric[7].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[8].name, "model_name", sizeof(message.metric[8].name));
        strncpy(message.metric[8].string_value, "Nucleo-F767zi", sizeof(message.metric[8].string_value));
        message.metric[8].has_string_value = true;
        message.metric[8].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[9].name, "cpu_version", sizeof(message.metric[9].name));
        strncpy(message.metric[9].string_value, "STM32F767ZI", sizeof(message.metric[9].string_value));
        message.metric[9].has_string_value = true;
        message.metric[9].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[10].name, "connection_ip", sizeof(message.metric[10].name));
        strncpy(message.metric[10].string_value, dhcp_address, sizeof(message.metric[10].string_value));
        message.metric[10].has_string_value = true;
        message.metric[10].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        /* LOL */
        strncpy(message.metric[11].name, "available_processors", sizeof(message.metric[11].name));
        strncpy(message.metric[11].string_value, "1", sizeof(message.metric[11].string_value));
        message.metric[11].has_string_value = true;
        message.metric[11].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        /* SRAM */
        strncpy(message.metric[12].name, "total_memory", sizeof(message.metric[12].name));
        strncpy(message.metric[12].string_value, "524288", sizeof(message.metric[12].string_value));
        message.metric[12].has_string_value = true;
        message.metric[12].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        /* https://docs.zephyrproject.org/latest/reference/kernel/other/version.html */
        uint32_t kernel_version = sys_kernel_version_get();
        #define KERNEL_VERSION_STRING_MAX 16
        char kernel_version_string[KERNEL_VERSION_STRING_MAX + 1] = "";
        snprintf(kernel_version_string, KERNEL_VERSION_STRING_MAX, "%d.%d.%d",
            SYS_KERNEL_VER_MAJOR(kernel_version),
            SYS_KERNEL_VER_MINOR(kernel_version),
            SYS_KERNEL_VER_PATCHLEVEL(kernel_version)
        );
        strncpy(message.metric[13].name, "os_version", sizeof(message.metric[13].name));
        strncpy(message.metric[13].string_value, kernel_version_string, sizeof(message.metric[13].string_value));
        message.metric[13].has_string_value = true;
        message.metric[13].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[14].name, "serial_number", sizeof(message.metric[14].name));
        strncpy(message.metric[14].string_value, device_id_string, sizeof(message.metric[14].string_value));
        message.metric[14].has_string_value = true;
        message.metric[14].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[15].name, "model_id", sizeof(message.metric[15].name));
        strncpy(message.metric[15].string_value, "Nucleo-F767zi", sizeof(message.metric[15].string_value));
        message.metric[15].has_string_value = true;
        message.metric[15].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[16].name, "bios_version", sizeof(message.metric[16].name));
        /* TODO: Determine a way to place the MCUBoot version in this string */
        /* Per https://github.com/mcu-tools/mcuboot/issues/1079 this data is unavailable at this time */
        /* This is GitLab wishlist issue #12 https://gitlab.com/SpringCitySolutionsLLC/zephyr-morsecode/-/issues/12 */
        strncpy(message.metric[16].string_value, image_build_time, sizeof(message.metric[16].string_value));
        message.metric[16].has_string_value = true;
        message.metric[16].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[17].name, "part_number", sizeof(message.metric[17].name));
        strncpy(message.metric[17].string_value, "STM32F767ZI", sizeof(message.metric[17].string_value));
        message.metric[17].has_string_value = true;
        message.metric[17].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        strncpy(message.metric[18].name, "connection_interface", sizeof(message.metric[18].name));
        strncpy(message.metric[18].string_value, mac_address_string, sizeof(message.metric[18].string_value));
        message.metric[18].has_string_value = true;
        message.metric[18].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_STRING;

        message.metric_count = 19;
        bool status = pb_encode(&stream, kuradatatypes_KuraPayload_fields, &message);
	    if (!status) {
            LOG_ERR("mqtt_kapua_birthcertificate: Encoding Failed: [%s]", PB_GET_ERROR(&stream));
	    }
        payload_length = stream.bytes_written; /* NOT a null terminated string LOL */
        LOG_DBG("mqtt_kapua_birthcertificate: Constructed MQTT payload of length: [%d]", payload_length);
        LOG_DBG("mqtt_kapua_birthcertificate: Constructed MQTT payload of: [%s]", payload);

	    struct mqtt_publish_param param;
	    param.message.topic.qos = MQTT_QOS_2_EXACTLY_ONCE;
	    param.message.topic.topic.utf8 = (uint8_t *) topic;
	    param.message.topic.topic.size = strlen(param.message.topic.topic.utf8);
	    param.message.payload.data = payload;
	    param.message.payload.len = payload_length;
	    param.message_id = sys_rand32_get();
	    param.dup_flag = 0U;
	    param.retain_flag = 0U;

	    rc = mqtt_publish(&client_ctx, &param);
        LOG_DBG("mqtt_kapua_birthcertificate: publish result : %d", rc);

        /* Do NOT forget to free the malloc */
        free(topic);
        free(payload);
    }

    return 0;
}

int mqtt_kapua_connect(void)
{
    int rc;

    /* If the MQTT server is not configured, do not try to talk to it. */
    while (!strcmp(mqtt_address, "0.0.0.0")) {
        return 0;
    }

    rc = mqtt_try_to_connect(&client_ctx);
    LOG_DBG("mqtt_kapua_connect result : %d", rc);

    return rc;
}

/* TODO: run this before executing a remote system reset/reboot */
/* TODO: Send Kapua a Death Certificate to mark the device as disconnected */
/* This is GitLab wishlist issue #13 https://gitlab.com/SpringCitySolutionsLLC/zephyr-morsecode/-/issues/13 */
int mqtt_kapua_disconnect(void)
{
    int rc;

    if (connected)
    {
        rc = mqtt_disconnect(&client_ctx);
        LOG_DBG("mqtt_kapua_disconnect result : %d", rc);

        return 0;
    }

    return rc;
}

int mqtt_kapua_publish(uint32_t transmissions)
{
    int rc;

    /* If the MQTT server is not configured, do not try to talk to it. */
    while (!strcmp(mqtt_address, "0.0.0.0")) {
        return 0;
    }

    if (connected)
    {
        /* Do NOT forget to free the malloc */
        #define KAPUA_PUBLISH_TOPIC_MAX 128
        uint8_t *topic = malloc(KAPUA_PUBLISH_TOPIC_MAX);
        snprintf(topic, KAPUA_PUBLISH_TOPIC_MAX, "%s/%s/%s", kapua_tenant, hostname, kapua_appname);
        /* A very typical result might be "scs/f767zi/morsecode" */
        LOG_DBG("mqtt_kapua_publish: Constructed MQTT topic name of: [%s]", topic);

        /* Do NOT forget to free the malloc */
        #define KAPUA_PUBLISH_PAYLOAD_MAX 128
        uint8_t *payload = malloc(KAPUA_PUBLISH_PAYLOAD_MAX);
        memset(payload, 0, KAPUA_PUBLISH_PAYLOAD_MAX); /* Otherwise testing dumps of the buffer will contain confusing old hawkBit stuff */
        size_t payload_length; /* Is a binary buffer maybe containing 0x00, not a null terminated string */
        kuradatatypes_KuraPayload message = kuradatatypes_KuraPayload_init_default;
        pb_ostream_t stream = pb_ostream_from_buffer(payload, KAPUA_PUBLISH_PAYLOAD_MAX);
        message.metric[0].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_INT32;
        message.metric[0].int_value = transmissions;
        message.metric[0].has_int_value = true; /* Need this or it will not include the value */
        strncpy(message.metric[0].name, "transmissions", sizeof(message.metric[0].name));
        message.metric[1].type = kuradatatypes_KuraPayload_KuraMetric_ValueType_FLOAT;
        message.metric[1].float_value = (float) (k_uptime_get() / (float) 3600000.0);
        message.metric[1].has_float_value = true; /* Need this or it will not include the value */
        strncpy(message.metric[1].name, "uptime", sizeof(message.metric[1].name));
        message.metric_count = 2; /* This is completely undocumented yet in retrospect seems obvious? */
        bool status = pb_encode(&stream, kuradatatypes_KuraPayload_fields, &message);
	    if (!status) {
            LOG_ERR("mqtt_kapua_publish: Encoding Failed: [%s]", PB_GET_ERROR(&stream));
	    }
        payload_length = stream.bytes_written; /* NOT a null terminated string LOL */
        LOG_DBG("mqtt_kapua_publish: Constructed MQTT payload of length: [%d]", payload_length);
        LOG_DBG("mqtt_kapua_publish: Constructed MQTT payload of: [%s]", payload);

	    struct mqtt_publish_param param;
	    param.message.topic.qos = MQTT_QOS_2_EXACTLY_ONCE;
	    param.message.topic.topic.utf8 = (uint8_t *) topic;
	    param.message.topic.topic.size = strlen(param.message.topic.topic.utf8);
	    param.message.payload.data = payload;
	    param.message.payload.len = payload_length;
	    param.message_id = sys_rand32_get();
	    param.dup_flag = 0U;
	    param.retain_flag = 0U;

	    rc = mqtt_publish(&client_ctx, &param);
        LOG_DBG("mqtt_kapua_publish: publish result : %d", rc);

        /* Do NOT forget to free the malloc */
        free(topic);
        free(payload);
    }

    return 0;
}

int mqtt_process_mqtt_and_sleep(struct mqtt_client *client, int timeout)
{
	int64_t remaining = timeout;
	int64_t start_time = k_uptime_get();
	int rc;

	while (remaining > 0 && connected) {
		if (mqtt_wait(remaining)) {
			rc = mqtt_input(client);
			if (rc != 0) {
                LOG_INF("mqtt_process_mqtt_and_sleep: mqtt_input: %d", rc);
				return rc;
			}
		}

		rc = mqtt_live(client);
		if (rc != 0 && rc != -EAGAIN) {
            LOG_INF("mqtt_process_mqtt_and_sleep: mqtt_live: %d", rc);
			return rc;
		} else if (rc == 0) {
			rc = mqtt_input(client);
			if (rc != 0) {
                LOG_INF("mqtt_process_mqtt_and_sleep: mqtt_input: %d", rc);
				return rc;
			}
		}

		remaining = timeout + start_time - k_uptime_get();
	}

	return 0;
}

int mqtt_publish_transmissions(uint32_t transmissions)
{
    int rc;

    /* If the MQTT server is not configured, do not try to talk to it. */
    while (!strcmp(mqtt_address, "0.0.0.0")) {
        return 0;
    }

    if (connected)
    {
        /* Do NOT forget to free the malloc */
        #define MQTT_PUBLISH_TRANSMISSIONS_TOPIC_MAX 128
        uint8_t *topic = malloc(MQTT_PUBLISH_TRANSMISSIONS_TOPIC_MAX);
        snprintf(topic, MQTT_PUBLISH_TRANSMISSIONS_TOPIC_MAX, "%s/%s/%s/transmissions", kapua_tenant, hostname, kapua_appname);
        /* A very typical result might be "scs/f767zi/morsecode/transmissions" */
        LOG_DBG("mqtt_publish_transmissions: Constructed MQTT topic name of: [%s]", topic);

        /* Do NOT forget to free the malloc */
        #define MQTT_PUBLISH_TRANSMISSIONS_PAYLOAD_MAX 128
        uint8_t *payload = malloc(MQTT_PUBLISH_TRANSMISSIONS_PAYLOAD_MAX);
        snprintf(payload, MQTT_PUBLISH_TRANSMISSIONS_PAYLOAD_MAX, "%d", transmissions);
        LOG_DBG("mqtt_publish_transmissions: Constructed MQTT payload of: [%s]", payload);

	    struct mqtt_publish_param param;
	    param.message.topic.qos = MQTT_QOS_2_EXACTLY_ONCE;
	    param.message.topic.topic.utf8 = (uint8_t *) topic;
	    param.message.topic.topic.size = strlen(param.message.topic.topic.utf8);
	    param.message.payload.data = payload;
	    param.message.payload.len = strlen(param.message.payload.data);
	    param.message_id = sys_rand32_get();
	    param.dup_flag = 0U;
	    param.retain_flag = 0U;

	    rc = mqtt_publish(&client_ctx, &param);
        LOG_DBG("mqtt_publish_transmissions: publish result : %d", rc);

        /* Do NOT forget to free the malloc */
        free(topic);
        free(payload);
    }

    return 0;
}

int mqtt_publish_uptime(void)
{
    int rc;

    /* If the MQTT server is not configured, do not try to talk to it. */
    while (!strcmp(mqtt_address, "0.0.0.0")) {
        return 0;
    }

    if (connected)
    {
        /* Do NOT forget to free the malloc */
        #define MQTT_PUBLISH_UPTIME_TOPIC_MAX 128
        uint8_t *topic = malloc(MQTT_PUBLISH_UPTIME_TOPIC_MAX);
        snprintf(topic, MQTT_PUBLISH_UPTIME_TOPIC_MAX, "%s/%s/%s/uptime", kapua_tenant, hostname, kapua_appname);
        /* A very typical result might be "scs/f767zi/morsecode/uptime" */
        LOG_DBG("mqtt_publish_uptime: Constructed MQTT topic name of: [%s]", topic);

        /* Do NOT forget to free the malloc */
        #define MQTT_PUBLISH_UPTIME_PAYLOAD_MAX 128
        uint8_t *payload = malloc(MQTT_PUBLISH_UPTIME_PAYLOAD_MAX);
        float uptime = k_uptime_get() / 3600000.0;
        snprintf(payload, MQTT_PUBLISH_UPTIME_PAYLOAD_MAX, "%0.2f", uptime);
        LOG_DBG("mqtt_publish_uptime: Constructed MQTT payload of: [%s]", payload);

	    struct mqtt_publish_param param;
	    param.message.topic.qos = MQTT_QOS_2_EXACTLY_ONCE;
	    param.message.topic.topic.utf8 = (uint8_t *) topic;
	    param.message.topic.topic.size = strlen(param.message.topic.topic.utf8);
	    param.message.payload.data = payload;
	    param.message.payload.len = strlen(param.message.payload.data);
	    param.message_id = sys_rand32_get();
	    param.dup_flag = 0U;
	    param.retain_flag = 0U;

	    rc = mqtt_publish(&client_ctx, &param);
        LOG_DBG("mqtt_publish_uptime: publish result : %d", rc);

        /* Do NOT forget to free the malloc */
        free(topic);
        free(payload);
    }

    return 0;
}

void mqtt_thread(void)
{

	int rc;

	/* Connect to Eclipse Kapua */
	k_msleep(100);
	mqtt_kapua_connect();
	k_msleep(100);
	mqtt_kapua_birthcertificate();

	while (1) {

        /* If there is new data to transmit according to the semaphore, then send it */
        if (k_sem_take(&send_data_sem, K_NO_WAIT) == 0){
            /* This is the simplistic plain-old string MQTT sender */
		    int mqtt_publish_transmissions_rc = mqtt_publish_transmissions(transmissions);
		    LOG_DBG("mqtt_publish_transmissions_rc: [%d]", mqtt_publish_transmissions_rc);
            rc = mqtt_process_mqtt_and_sleep(&client_ctx, 100);
            LOG_DBG("mqtt_thread: mqtt_process_mqtt_and_sleep result : %d", rc);
		    int mqtt_publish_uptime_rc = mqtt_publish_uptime();
		    LOG_DBG("mqtt_publish_uptime_rc: [%d]", mqtt_publish_uptime_rc);
            rc = mqtt_process_mqtt_and_sleep(&client_ctx, 100);
            LOG_DBG("mqtt_thread: mqtt_process_mqtt_and_sleep result : %d", rc);

		    /* This is the Kapua protobuf sender */
		    /* Feel free to leave both uncommented, although */
		    /* in production use you would probably only use the protobuf */
		    int mqtt_kapua_publish_rc = mqtt_kapua_publish(transmissions);
            LOG_DBG("mqtt_kapua_publish_rc: [%d]", mqtt_kapua_publish_rc);
            rc = mqtt_process_mqtt_and_sleep(&client_ctx, 100);
            LOG_DBG("mqtt_thread: mqtt_process_mqtt_and_sleep result : %d", rc);
        }

        /* Periodic Processing of MQTT packets */
        /* Both processing new incoming packets AND responding to MQTT PING pkts */
        /* Note the device and Kapua are on the same LAN so decreasing delay from 500 ms */
        rc = mqtt_process_mqtt_and_sleep(&client_ctx, 100);
        LOG_DBG("mqtt_thread: mqtt_process_mqtt_and_sleep result : %d", rc);

        k_msleep(1000);
    }

}

int mqtt_try_to_connect(struct mqtt_client *client)
{
    int rc, i = 0;

    /* I guess we try to connect 20 times because that seems reasonable */
    while (i++ < 20 && !connected)
    {

        mqtt_client_start(client);

        rc = mqtt_connect(client);
        if (rc != 0)
        {
            LOG_ERR("mqtt_connect rc : %d", rc);
            /* Wait a second before trying again */
            k_sleep(K_MSEC(1000));
            continue;
        }

        fds[0].fd = client->transport.tcp.sock;
        fds[0].events = ZSOCK_POLLIN;
        nfds = 1;

        /* The demo app waits 2 seconds, I donno why */
        if (mqtt_wait(2000))
        {
            mqtt_input(client);
        }

        if (!connected)
        {
            mqtt_abort(client);
        }
    }

    if (connected)
    {
        return 0;
    }

    return -EINVAL;
}

/* This polling code is from the MQTT example */
int mqtt_wait(int timeout)
{
    int ret = 0;

    if (nfds > 0)
    {
        ret = zsock_poll(fds, nfds, timeout);
        if (ret < 0)
        {
            LOG_ERR("poll error: %d", errno);
        }
    }

    return ret;
}
