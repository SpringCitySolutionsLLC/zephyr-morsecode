/*
 * morse.c
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021-2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/stats/stats.h>

#include "main.h"
#include "morse.h"
#include "mqtt.h"

#define LOG_LEVEL LOG_LEVEL_INF
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(morsecode_morse);

/* Some time later on we will record stats on how many total */
/* dots and dashes have been transmitted, but for now this is */
/* simplistic placeholder to merely demonstrate mcumgr. */
/* transmissions is how many times the entire message was blinked out */
STATS_SECT_START(morsecode_stats)
STATS_SECT_ENTRY(transmissions)
STATS_SECT_END;
STATS_NAME_START(morsecode_stats)
STATS_NAME(morsecode_stats, transmissions)
STATS_NAME_END(morsecode_stats);
STATS_SECT_DECL(morsecode_stats) morsecode_stats;

/* In video #301 we will simply use led0 */
/* In video #302 there will be devicetree overlay activity */
#define LED0_NODE DT_ALIAS(led0)

static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);

void morse_thread(void)
{
	/* If you rebooted at the wrong instant, this could be necessary? */
	off();

	/* Start Stats */
	int stats_rc = STATS_INIT_AND_REG(morsecode_stats, STATS_SIZE_32, "morsecode_stats");
	if (stats_rc < 0) {
		LOG_ERR("Error initializing stats system [%d]", stats_rc);
	}

	while (1) {
		morse_send_message();

		LOG_DBG("Transmission Completed.");

		/* Increment the transmissions counter */
		STATS_INC(morsecode_stats, transmissions);
		transmissions++;

		/* Tell MQTT there is new data to transmit via a semaphore */
		k_sem_give(&send_data_sem);

		/* TODO: This delay between messages could be made configurable */
		k_msleep(5000);
	}
}

/* This function is morse only in the sense of */
/* if it involves something other than dots, dashes, and spaces, */
/* then it belongs elsewhere. */
void morse_send_message(void)
{
	letter_a();
	letter_time();
	letter_u();
	letter_time();
	letter_t();
	letter_time();
	letter_o();
	letter_time();
	letter_m();
	letter_time();
	letter_a();
	letter_time();
	letter_t();
	letter_time();
	letter_i();
	letter_time();
	letter_o();
	letter_time();
	letter_n();
	word_time();
	number_3();
	letter_time();
	number_0();
	letter_time();
	number_6();
	letter_time();
	word_time();

	return;
}

void on()
{
	int ret;

	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return;
	}
	return;
}

void off()
{
	int ret;

	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_INACTIVE);
	if (ret < 0) {
		return;
	}
	return;
}

void dit(void)
{
	on();
	dit_time();
	off();
	return;
}

void dash(void)
{
	on();
	letter_time();
	off();
	return;
}

void dit_time(void)
{
	k_msleep(dot_ms);
	return;
}

void letter_time(void)
{
	/* I was calling three dit times to sleep. */
	/* However the RTOS can schedule better with a longer sleep rather than many small sleeps. */
	k_msleep(3 * dot_ms);
	return;
}

void word_time(void)
{
	k_msleep(7 * dot_ms);
	return;
}

void letter_a(void)
{
	dit();
	dit_time();
	dash();
	return;
}

void letter_b(void)
{
	dash();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void letter_c(void)
{
	dash();
	dit_time();
	dit();
	dit_time();
	dash();
	dit_time();
	dit();
	return;
}

void letter_d(void)
{
	dash();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void letter_e(void)
{
	dit();
	return;
}

void letter_f(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dash();
	dit_time();
	dit();
	return;
}

void letter_g(void)
{
	dash();
	dit_time();
	dash();
	dit_time();
	dit();
	return;
}

void letter_h(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void letter_i(void)
{
	dit();
	dit_time();
	dit();
	return;
}

void letter_j(void)
{
	dit();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	return;
}

void letter_k(void)
{
	dash();
	dit_time();
	dit();
	dit_time();
	dash();
	return;
}

void letter_l(void)
{
	dit();
	dit_time();
	dash();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void letter_m(void)
{
	dash();
	dit_time();
	dash();
	return;
}

void letter_n(void)
{
	dash();
	dit_time();
	dit();
	return;
}

void letter_o(void)
{
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	return;
}

void letter_p(void)
{
	dit();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dit();
	return;
}

void letter_q(void)
{
	dash();
	dit_time();
	dash();
	dit_time();
	dit();
	dit_time();
	dash();
	return;
}

void letter_r(void)
{
	dit();
	dit_time();
	dash();
	dit_time();
	dit();
	return;
}

void letter_s(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void letter_t(void)
{
	dash();
	return;
}

void letter_u(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dash();
	return;
}

void letter_v(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dash();
	return;
}

void letter_w(void)
{
	dit();
	dit_time();
	dash();
	dit_time();
	dash();
	return;
}

void letter_x(void)
{
	dash();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dash();
	return;
}

void letter_y(void)
{
	dash();
	dit_time();
	dit();
	dit_time();
	dash();
	dit_time();
	dash();
	return;
}

void letter_z(void)
{
	dash();
	dit_time();
	dash();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void number_1(void)
{
	dit();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	return;
}

void number_2(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	return;
}

void number_3(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dash();
	dit_time();
	dash();
	return;
}

void number_4(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dash();
	return;
}

void number_5(void)
{
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void number_6(void)
{
	dash();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void number_7(void)
{
	dash();
	dit_time();
	dash();
	dit_time();
	dit();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void number_8(void)
{
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dit();
	dit_time();
	dit();
	return;
}

void number_9(void)
{
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dit();
	return;
}

void number_0(void)
{
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	dit_time();
	dash();
	return;
}
