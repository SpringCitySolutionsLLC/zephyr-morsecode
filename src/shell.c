/*
 * shell.c
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021-2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>

#include <zephyr/kernel.h>
#include <zephyr/shell/shell.h>

#include "main.h"
#include "settings.h"

static int cmd_hostname(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "Hostname:[%s]", hostname);
	}

	if (argc == 2) {
		shell_print(shell, "Hostname was:[%s]", hostname);
		strncpy(hostname, argv[1], HOSTNAME_MAX);
		settings_save_hostname();
		shell_print(shell, "Hostname is now:[%s]", hostname);
	}

	return 0;
}

SHELL_CMD_REGISTER(hostname, NULL, "Get / Set Hostname", cmd_hostname);

static int cmd_kapua_appname(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "Kapua App Name:[%s]", kapua_appname);
	}

	if (argc == 2) {
		shell_print(shell, "Kapua App Name was:[%s]", kapua_appname);
		strncpy(kapua_appname, argv[1], KAPUA_APPNAME_MAX);
		settings_save_kapua_appname();
		shell_print(shell, "Kapua App Name is now:[%s]", kapua_appname);
	}

	return 0;
}

SHELL_CMD_REGISTER(kapua_appname, NULL, "Get / Set Kapua App Name", cmd_kapua_appname);

static int cmd_kapua_displayname(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "Kapua Display Name:[%s]", kapua_displayname);
	}

	if (argc == 2) {
		shell_print(shell, "Kapua Display Name was:[%s]", kapua_displayname);
		strncpy(kapua_displayname, argv[1], KAPUA_DISPLAYNAME_MAX);
		settings_save_kapua_displayname();
		shell_print(shell, "Kapua Display Name is now:[%s]", kapua_displayname);
	}

	return 0;
}

SHELL_CMD_REGISTER(kapua_displayname, NULL, "Get / Set Kapua Display Name", cmd_kapua_displayname);

static int cmd_kapua_tenant(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "Kapua Tenant:[%s]", kapua_tenant);
	}

	if (argc == 2) {
		shell_print(shell, "Kapua Tenant was:[%s]", kapua_tenant);
		strncpy(kapua_tenant, argv[1], KAPUA_TENANT_MAX);
		settings_save_kapua_tenant();
		shell_print(shell, "Kapua Tenant is now:[%s]", kapua_tenant);
	}

	return 0;
}

SHELL_CMD_REGISTER(kapua_tenant, NULL, "Get / Set Kapua Tenant", cmd_kapua_tenant);

static int cmd_mqtt_address(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MQTT Address:[%s]", mqtt_address);
	}

	if (argc == 2) {
		shell_print(shell, "MQTT Address was:[%s]", mqtt_address);
		strncpy(mqtt_address, argv[1], MQTT_ADDRESS_MAX);
		settings_save_mqtt_address();
		shell_print(shell, "MQTT Address is now:[%s]", mqtt_address);
	}

	return 0;
}

SHELL_CMD_REGISTER(mqtt_address, NULL, "Get / Set MQTT Address", cmd_mqtt_address);

static int cmd_mqtt_clientid(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MQTT Client ID:[%s]", mqtt_clientid);
	}

	if (argc == 2) {
		shell_print(shell, "MQTT Client ID was:[%s]", mqtt_clientid);
		strncpy(mqtt_clientid, argv[1], MQTT_CLIENTID_MAX);
		settings_save_mqtt_clientid();
		shell_print(shell, "MQTT Client ID is now:[%s]", mqtt_clientid);
	}

	return 0;
}

SHELL_CMD_REGISTER(mqtt_clientid, NULL, "Get / Set MQTT Client ID", cmd_mqtt_clientid);

static int cmd_dhcp_address(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "DHCP assigned address:[%s]", dhcp_address);
	}

	return 0;
}

SHELL_CMD_REGISTER(dhcp_address, NULL, "Get DHCP Address", cmd_dhcp_address);

static int cmd_mac_1(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MAC byte 1:[%s]", mac_1);
	}

	if (argc == 2) {
		shell_print(shell, "MAC byte 1 was:[%s]", mac_1);
		strncpy(mac_1, argv[1], MAC_1_MAX);
		settings_save_mac_1();
		shell_print(shell, "MAC byte 1 is now:[%s]", mac_1);
	}

	return 0;
}

SHELL_CMD_REGISTER(mac_1, NULL, "Get / Set MAC byte 1", cmd_mac_1);

static int cmd_mac_2(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MAC byte 2:[%s]", mac_2);
	}

	if (argc == 2) {
		shell_print(shell, "MAC byte 2 was:[%s]", mac_2);
		strncpy(mac_2, argv[1], MAC_2_MAX);
		settings_save_mac_2();
		shell_print(shell, "MAC byte 2 is now:[%s]", mac_2);
	}

	return 0;
}

SHELL_CMD_REGISTER(mac_2, NULL, "Get / Set MAC byte 2", cmd_mac_2);

static int cmd_mac_3(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MAC byte 3:[%s]", mac_3);
	}

	if (argc == 2) {
		shell_print(shell, "MAC byte 3 was:[%s]", mac_3);
		strncpy(mac_3, argv[1], MAC_3_MAX);
		settings_save_mac_3();
		shell_print(shell, "MAC byte 3 is now:[%s]", mac_3);
	}

	return 0;
}

SHELL_CMD_REGISTER(mac_3, NULL, "Get / Set MAC byte 3", cmd_mac_3);

static int cmd_mac_4(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MAC byte 4:[%s]", mac_4);
	}

	if (argc == 2) {
		shell_print(shell, "MAC byte 4 was:[%s]", mac_4);
		strncpy(mac_4, argv[1], MAC_4_MAX);
		settings_save_mac_4();
		shell_print(shell, "MAC byte 4 is now:[%s]", mac_4);
	}

	return 0;
}

SHELL_CMD_REGISTER(mac_4, NULL, "Get / Set MAC byte 4", cmd_mac_4);

static int cmd_mac_5(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MAC byte 5:[%s]", mac_5);
	}

	if (argc == 2) {
		shell_print(shell, "MAC byte 5 was:[%s]", mac_5);
		strncpy(mac_5, argv[1], MAC_5_MAX);
		settings_save_mac_5();
		shell_print(shell, "MAC byte 5 is now:[%s]", mac_5);
	}

	return 0;
}

SHELL_CMD_REGISTER(mac_5, NULL, "Get / Set MAC byte 5", cmd_mac_5);

static int cmd_mac_6(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MAC byte 6:[%s]", mac_6);
	}

	if (argc == 2) {
		shell_print(shell, "MAC byte 6 was:[%s]", mac_6);
		strncpy(mac_6, argv[1], MAC_6_MAX);
		settings_save_mac_6();
		shell_print(shell, "MAC byte 6 is now:[%s]", mac_6);
	}

	return 0;
}

SHELL_CMD_REGISTER(mac_6, NULL, "Get / Set MAC byte 6", cmd_mac_6);

static int cmd_mac_address(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MAC address:[%s]", mac_address_string);
	}

	return 0;
}

SHELL_CMD_REGISTER(mac_address, NULL, "Get MAC Address", cmd_mac_address);

static int cmd_mqtt_pword(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MQTT Password:[%s]", mqtt_pword);
	}

	if (argc == 2) {
		shell_print(shell, "MQTT Password was:[%s]", mqtt_pword);
		strncpy(mqtt_pword, argv[1], MQTT_PWORD_MAX);
		settings_save_mqtt_pword();
		shell_print(shell, "MQTT Password is now:[%s]", mqtt_pword);
	}

	return 0;
}

SHELL_CMD_REGISTER(mqtt_pword, NULL, "Get / Set MQTT Password", cmd_mqtt_pword);

static int cmd_mqtt_uname(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "MQTT Username:[%s]", mqtt_uname);
	}

	if (argc == 2) {
		shell_print(shell, "MQTT Username was:[%s]", mqtt_uname);
		strncpy(mqtt_uname, argv[1], MQTT_UNAME_MAX);
		settings_save_mqtt_uname();
		shell_print(shell, "MQTT Username is now:[%s]", mqtt_uname);
	}

	return 0;
}

SHELL_CMD_REGISTER(mqtt_uname, NULL, "Get / Set MQTT Username", cmd_mqtt_uname);

static int cmd_uptime(const struct shell *shell, size_t argc, char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

		float uptime = k_uptime_get() / 3600000.0;
		char buffer[40];
		sprintf(buffer, "%0.2f", uptime);
		shell_print(shell, "Uptime in Hours:[%s]", buffer);
        return 0;
}

SHELL_CMD_REGISTER(uptime, NULL, "Get Uptime in Hours", cmd_uptime);

static int cmd_version(const struct shell *shell, size_t argc, char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

        shell_print(shell, "Firmware Build Timestamp:[%s]", image_build_time);
        return 0;
}

SHELL_CMD_REGISTER(version, NULL, "Get Firmware Build Timestamp", cmd_version);

static int cmd_wifi_ssid(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "WiFi SSID:[%s]", wifi_ssid);
	}

	if (argc == 2) {
		shell_print(shell, "WiFi SSID was:[%s]", wifi_ssid);
		strncpy(wifi_ssid, argv[1], WIFI_SSID_MAX);
		settings_save_wifi_ssid();
		shell_print(shell, "WiFi SSID is now:[%s]", wifi_ssid);
	}

	return 0;
}

SHELL_CMD_REGISTER(wifi_ssid, NULL, "Get / Set WiFi SSID", cmd_wifi_ssid);

static int cmd_wifi_password(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "WiFi Password:[%s]", wifi_password);
	}

	if (argc == 2) {
		shell_print(shell, "WiFi Password was:[%s]", wifi_password);
		strncpy(wifi_password, argv[1], WIFI_PASSWORD_MAX);
		settings_save_wifi_password();
		shell_print(shell, "WiFi Password is now:[%s]", wifi_password);
	}

	return 0;
}

SHELL_CMD_REGISTER(wifi_password, NULL, "Get / Set WiFi Password", cmd_wifi_password);

static int cmd_wpm(const struct shell *shell, size_t argc, char **argv)
{

	if (argc == 1) {
		shell_print(shell, "Words Per Minute:[%s]", wpm);
	}

	if (argc == 2) {
		shell_print(shell, "Words Per Minute was:[%s]", wpm);
		int new_wpm = strtol(argv[1], NULL, 10);
		if (new_wpm > 4 && new_wpm < 140){
			dot_ms = 60000 / (50 * new_wpm);
			strncpy(wpm, argv[1], WPM_MAX);
			settings_save_wpm();
		}
		shell_print(shell, "Words Per Minute is now:[%s]", wpm);
	}

	return 0;
}

SHELL_CMD_REGISTER(wpm, NULL, "Get / Set Words Per Minute", cmd_wpm);
