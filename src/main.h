/*
 * main.h
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021-2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MAIN_H
#define MAIN_H

/* Device ID as a hexadecimal string */
/* This should match the output of shell command "hwinfo devid" */
/* Really needs to be more than twice as long as (main.c) DEVICE_ID_BINARY_MAX */
/* Write in main.c at boot */
/* Read in settings.c where the last three hex digits will be the last three default MAC addr bytes */
/* Read in mqtt.c mqtt_kapua_birthcertificate() */
#define DEVICE_ID_STRING_MAX 31
extern char device_id_string[DEVICE_ID_STRING_MAX + 1];

/* The IPv4 address provided by DHCP */
/* Write in dhcp.c */
/* Read in main.c for logging */
/* Read in mqtt.c mqtt_kapua_birthcertificate() */
/* Read in shell.c */
#define DHCP_ADDRESS_MAX 23
extern char dhcp_address[DHCP_ADDRESS_MAX + 1];

/* A calculated global setting based on the wpm variable */
/* Write in shell.c */
/* Read in morse.c */ 
/* dot time in milliseconds at 13 WPM would be 60000 / (50 * 13) = 92 */
/* At least as per: */
/* https://morsecode.world/international/timing.html */
extern int dot_ms;

/* This was going to be very useful for mDNS and shell command prompt */
/* A flash-savable setting */
/* Read and Write in shell.c via the hostname command */
/* Read in mqtt.c */
#define HOSTNAME_MAX 31
extern char hostname[HOSTNAME_MAX + 1];

/* A string timestamp of the last time "west build -p" was run */
/* It is essentially the firmware version */
/* Read in shell.c by version command */
/* Read in mqtt.c */
#define IMAGE_BUILD_TIME_MAX 63
extern char image_build_time[IMAGE_BUILD_TIME_MAX + 1];

/* MAC Address byte 1 of 6 in two hex digit string format */
/* A flash-savable setting */
/* Read and Write in shell.c via the mac1 command */
/* Read in dhcp.c to set the MAC address */
#define MAC_1_MAX 3
extern char mac_1[MAC_1_MAX + 1];

/* MAC Address byte 2 of 6 in two hex digit string format */
/* A flash-savable setting */
/* Read and Write in shell.c via the mac1 command */
/* Read in dhcp.c to set the MAC address */
#define MAC_2_MAX 3
extern char mac_2[MAC_2_MAX + 1];

/* MAC Address byte 3 of 6 in two hex digit string format */
/* A flash-savable setting */
/* Read and Write in shell.c via the mac1 command */
/* Read in dhcp.c to set the MAC address */
#define MAC_3_MAX 3
extern char mac_3[MAC_3_MAX + 1];

/* MAC Address byte 4 of 6 in two hex digit string format */
/* A flash-savable setting */
/* Read and Write in shell.c via the mac1 command */
/* Read in dhcp.c to set the MAC address */
#define MAC_4_MAX 3
extern char mac_4[MAC_4_MAX + 1];

/* MAC Address byte 5 of 6 in two hex digit string format */
/* A flash-savable setting */
/* Read and Write in shell.c via the mac1 command */
/* Read in dhcp.c to set the MAC address */
#define MAC_5_MAX 3
extern char mac_5[MAC_5_MAX + 1];

/* MAC Address byte 6 of 6 in two hex digit string format */
/* A flash-savable setting */
/* Read and Write in shell.c via the mac1 command */
/* Read in dhcp.c to set the MAC address */
#define MAC_6_MAX 3
extern char mac_6[MAC_6_MAX + 1];

/* The Kapua application name */
/* Configurable in the context of morsecode or morsecode-test or morsecode-dev or ? */
/* A flash-savable setting */
/* Read and Write in shell.c via the kapua_appname command */
/* Read in mqtt.c */
#define KAPUA_APPNAME_MAX 31
extern char kapua_appname[KAPUA_APPNAME_MAX + 1];

/* The Kapua Display Name as seen in Device tab */
/* A flash-savable setting */
/* Read and Write in shell.c via the kapua_displayname command */
/* Read in mqtt.c */
#define KAPUA_DISPLAYNAME_MAX 31
extern char kapua_displayname[KAPUA_DISPLAYNAME_MAX + 1];

/* The Kapua tenant name aka "account" */
/* A flash-savable setting */
/* Read and Write in shell.c via the kapua_tenant command */
/* Read in mqtt.c */
#define KAPUA_TENANT_MAX 15
extern char kapua_tenant[KAPUA_TENANT_MAX + 1];

/* The ethernet MAC address in printable string form */
/* Write in dhcp.c */
/* Read in mqtt.c mqtt_kapua_birthcertificate() */
/* Read in shell.c */
#define MAC_ADDRESS_STRING_MAX 24
extern char mac_address_string[MAC_ADDRESS_STRING_MAX + 1];

/* The IPv4 address of the MQTT broker */
/* A flash-savable setting */
/* Read and Write in shell.c via the mqtt_address command */
/* Read in mqtt.c */
#define MQTT_ADDRESS_MAX 23
extern char mqtt_address[MQTT_ADDRESS_MAX + 1];

/* The Kapua Client ID as seen in MQTT broker status */
/* A flash-savable setting */
/* Read and Write in shell.c via the mqtt_clientid command */
/* Read in mqtt.c */
#define MQTT_CLIENTID_MAX 31
extern char mqtt_clientid[MQTT_CLIENTID_MAX + 1];

/* The MQTT broker password (the "thing" login for Kapua) */
/* A flash-savable setting */
/* Read and Write in shell.c via the mqtt_pword command */
/* Read in mqtt.c */
#define MQTT_PWORD_MAX 31
extern char mqtt_pword[MQTT_PWORD_MAX + 1];

/* The MQTT broker username (the "thing" login for Kapua) */
/* A flash-savable setting */
/* Read and Write in shell.c via the mqtt_uname command */
/* Read in mqtt.c */
#define MQTT_UNAME_MAX 31
extern char mqtt_uname[MQTT_UNAME_MAX + 1];

/* The semaphore used by morse.c thread to tell mqtt.c thread to send telemetry */
/* Right about here, software engineers with strongly-typed high level language experience are */
/* trying to type something like "extern k_sem send_data_sem;" and getting VERY frustrated */
/* This is just a C-lanugage thing to get used to. */
extern struct k_sem send_data_sem;

/* Completed Transmission Count */
/* Write in morse.c */
/* Read in mqtt.c to report counts */
extern uint32_t transmissions;

/* Wifi SSID aka "network name" */
/* A flash-savable setting */
/* Read and Write in shell.c via the wifi_ssid command */
/* Read and Write in settings.c to store in flash */
/* Read in dhcp.c to configure the wifi */
#define WIFI_SSID_MAX 31
extern char wifi_ssid[WIFI_SSID_MAX + 1];

/* Wifi Password */
/* A flash-savable setting */
/* Read and Write in shell.c via the wpm command */
/* Read and Write in settings.c to store in flash */
/* Read in dhcp.c to configure the wifi */
#define WIFI_PASSWORD_MAX 63
extern char wifi_password[WIFI_PASSWORD_MAX + 1];

/* Words per Minute morse code sending speed */
/* A flash-savable setting */
/* Read and Write in shell.c via the wpm command */
#define WPM_MAX 7
extern char wpm[WPM_MAX + 1];

#endif
