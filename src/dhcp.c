/*
 * dhcp.c
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021-2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Source code strongly inspired by these excellent Apache 2.0 licensed references: */
/* https://docs.zephyrproject.org/latest/samples/net/dhcpv4_client/README.html */
/* https://docs.zephyrproject.org/latest/samples/subsys/mgmt/hawkbit/README.html */

#include <zephyr/logging/log.h>

#include <zephyr/kernel.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <zephyr/net/net_config.h>
#include <zephyr/net/net_if.h>
#include <zephyr/net/net_core.h>
#include <zephyr/net/net_context.h>
#include <zephyr/net/net_event.h>
#include <zephyr/net/net_mgmt.h>
#include <zephyr/net/ethernet.h>
#include <zephyr/net/ethernet_mgmt.h>

#include "main.h"

#define LOG_LEVEL LOG_LEVEL_INF
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(morsecode_dhcp);

static struct net_mgmt_event_callback mgmt_cb;

/* Semaphore to indicate a lease has been acquired */
static K_SEM_DEFINE(dhcp_got_address, 0, 1);

static void dhcp_net_event_ipv4_addr_add_handler(struct net_mgmt_event_callback *cb,
		    uint32_t mgmt_event,
		    struct net_if *iface)
{
	int i;
	bool notified = false;

	if (mgmt_event != NET_EVENT_IPV4_ADDR_ADD) {
		return;
	}

	for (i = 0; i < NET_IF_MAX_IPV4_ADDR; i++) {
		if (iface->config.ip.ipv4->unicast[i].addr_type !=
		    NET_ADDR_DHCP) {
			continue;
		}

		if (!notified) {
			k_sem_give(&dhcp_got_address);
			notified = true;
		}
		break;
	}
}

void dhcp_read_mac_address_string(struct net_if *iface)
{
		snprintf(mac_address_string, MAC_ADDRESS_STRING_MAX, 
		"%02X:%02X:%02X:%02X:%02X:%02X",
		net_if_get_link_addr(iface)->addr[0],
		net_if_get_link_addr(iface)->addr[1],
		net_if_get_link_addr(iface)->addr[2],
		net_if_get_link_addr(iface)->addr[3],
		net_if_get_link_addr(iface)->addr[4],
		net_if_get_link_addr(iface)->addr[5]
		);
}

void dhcp_startup(void)
{
	struct net_if *iface;
	uint8_t mac_addr_change[6] = { 0x02, 0x80, 0xE1, 0x01,  0x02,  0x03 };
	struct ethernet_req_params params;

	net_mgmt_init_event_callback(&mgmt_cb, dhcp_net_event_ipv4_addr_add_handler, NET_EVENT_IPV4_ADDR_ADD);
	net_mgmt_add_event_callback(&mgmt_cb);

	iface = net_if_get_default();

	/* This URL explains MAC address changes pretty well: */
	/* https://github.com/zephyrproject-rtos/zephyr/blob/main/tests/net/ethernet_mgmt/src/main.c */
	
	dhcp_read_mac_address_string(iface);
	LOG_DBG("dhcp_startup: Old MAC Address : %s", mac_address_string);

	mac_addr_change[0] = (char)strtol(mac_1, NULL, 16);
	mac_addr_change[1] = (char)strtol(mac_2, NULL, 16);
	mac_addr_change[2] = (char)strtol(mac_3, NULL, 16);
	mac_addr_change[3] = (char)strtol(mac_4, NULL, 16);
	mac_addr_change[4] = (char)strtol(mac_5, NULL, 16);
	mac_addr_change[5] = (char)strtol(mac_6, NULL, 16);

	net_if_down(iface);
	memcpy(params.mac_address.addr, mac_addr_change, 6);
	net_mgmt(NET_REQUEST_ETHERNET_SET_MAC_ADDRESS, iface, &params, sizeof(struct ethernet_req_params));
	net_if_up(iface);

	dhcp_read_mac_address_string(iface);
	LOG_DBG("dhcp_startup: New MAC Address : %s", mac_address_string);

	net_config_init_app(NULL, "Initializing Network");

	/* net_dhcpv4_start(iface); */

	/* Wait forever for a lease  ... Or should we? */
	k_sem_take(&dhcp_got_address, K_FOREVER);

	for (int i = 0; i < NET_IF_MAX_IPV4_ADDR; i++) {
		if (iface->config.ip.ipv4->unicast[i].addr_type == NET_ADDR_DHCP) {
			/* Save the addrs in a string */
			net_addr_ntop(AF_INET, &iface->config.ip.ipv4->unicast[i].address.in_addr, dhcp_address, DHCP_ADDRESS_MAX);
		}
	}

	LOG_DBG("Contents of string dhcp_address: [%s]", dhcp_address);

}