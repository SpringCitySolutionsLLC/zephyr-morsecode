/*
 * main.c
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021-2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <zephyr/kernel.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/mgmt/hawkbit.h>
#include <zephyr/dfu/mcuboot.h>
#include <zephyr/sys/reboot.h>
#include "os_mgmt/os_mgmt.h"
#include "img_mgmt/img_mgmt.h"
#include "stat_mgmt/stat_mgmt.h"
#include "shell_mgmt/shell_mgmt.h"

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(morsecode);

#include <zephyr/drivers/gpio.h>

#include <zephyr/settings/settings.h>
#include <syscalls/hwinfo.h>

#include "dhcp.h"
#include "main.h"
#include "mcumgr.h"
#include "morse.h"
#include "mqtt.h"
#include "settings.h"

/* Global variables */
int dot_ms = 92;
char device_id_string[DEVICE_ID_STRING_MAX + 1] = "";
char dhcp_address[DHCP_ADDRESS_MAX + 1] = "";
char hostname[HOSTNAME_MAX + 1] = "";
char image_build_time[IMAGE_BUILD_TIME_MAX + 1] = "";
char kapua_appname[KAPUA_APPNAME_MAX + 1] = "";
char kapua_displayname[KAPUA_DISPLAYNAME_MAX + 1] = "";
char kapua_tenant[KAPUA_TENANT_MAX + 1] = "";
char mac_1[MAC_1_MAX + 1] = "";
char mac_2[MAC_2_MAX + 1] = "";
char mac_3[MAC_3_MAX + 1] = "";
char mac_4[MAC_4_MAX + 1] = "";
char mac_5[MAC_5_MAX + 1] = "";
char mac_6[MAC_6_MAX + 1] = "";
char mac_address_string[MAC_ADDRESS_STRING_MAX + 1] = "";
char mqtt_address[MQTT_ADDRESS_MAX + 1] = "";
char mqtt_clientid[MQTT_CLIENTID_MAX + 1] = "";
char mqtt_pword[MQTT_PWORD_MAX + 1] = "";
char mqtt_uname[MQTT_UNAME_MAX + 1] = "";
uint32_t transmissions = 0;
char wifi_ssid[WIFI_SSID_MAX + 1] = "";
char wifi_password[WIFI_PASSWORD_MAX + 1] = "";
char wpm[WPM_MAX + 1] = "";

/* morse.c uses this semaphore to tell mqtt.c to send telemetry */
K_SEM_DEFINE(send_data_sem, 0, 1);

K_THREAD_DEFINE(mqtt_thread_id, 16384, mqtt_thread, NULL, NULL, NULL, 5, 0, 20000);
K_THREAD_DEFINE(morse_thread_id, 512, morse_thread, NULL, NULL, NULL, -10, 0, 25000);

void main(void)
{
	/* A nice side effect of logging the firmware build time is */
	/* we can determine which firmware has booted */
	/* Note you have to "west build -p" to force these macros to update, minorly annoying but whatever */
	snprintf(image_build_time, IMAGE_BUILD_TIME_MAX, "%s %s", __DATE__, __TIME__);
	LOG_INF("Image Build Time: [%s]", image_build_time);

	/* Zephyr does not seem to have a way to tell how long a device ID is. */
	/* Somehow the shell command "hwinfo devid" knows how long it is. */
	/* On my dev board, the cpu serial number is 12 bytes long. */
	/* Note that changes in the length of the serial number might result in */
	/* changes being required in the settings.c for default mac4, mac5, and mac6 */
	#define DEVICE_ID_BINARY_MAX 12
	char device_id_binary[DEVICE_ID_BINARY_MAX + 1];
    hwinfo_get_device_id(device_id_binary, DEVICE_ID_BINARY_MAX);
	for (int i = 0; i < DEVICE_ID_BINARY_MAX; i++)
	{
		snprintf(device_id_string + (i * 2), DEVICE_ID_STRING_MAX, "%02X", device_id_binary[i]);
	}
	LOG_INF("Device ID: [%s]", device_id_string);

	/* Read the stored settings, if any */
	settings_boot();
	int new_wpm = strtol(wpm, NULL, 10);
	if (new_wpm > 4 && new_wpm < 140){
		dot_ms = 60000 / (50 * new_wpm);
	}

	/* Set up network interface and get a DHCP address */
	dhcp_startup();
	k_msleep(1000);

	/* Connect to Eclipse hawkBit */
	hawkbit_init();
	hawkbit_autohandler();
	k_msleep(1000); /* Give hawkBit a fighting chance to upgrade firmware if there's a crash later on */

	/* Start MCUmgr */
	os_mgmt_register_group();
	img_mgmt_register_group();
	stat_mgmt_register_group();
	shell_mgmt_register_group();
	start_smp_udp();
	k_msleep(1000);

	/* TODO: Set the shell command line prompt to the variable hostname */
	/* This is GitLab wishlist issue #9 https://gitlab.com/SpringCitySolutionsLLC/zephyr-morsecode/-/issues/9 */

	/* Suspend this thread */
	k_sleep(K_FOREVER);
}
