/*
 * settings.h
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021-2022 Spring City Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

void settings_boot(void);
void settings_save_hostname(void);
void settings_save_kapua_appname(void);
void settings_save_kapua_displayname(void);
void settings_save_kapua_tenant(void);
void settings_save_mac_1(void);
void settings_save_mac_2(void);
void settings_save_mac_3(void);
void settings_save_mac_4(void);
void settings_save_mac_5(void);
void settings_save_mac_6(void);
void settings_save_mqtt_address(void);
void settings_save_mqtt_clientid(void);
void settings_save_mqtt_pword(void);
void settings_save_mqtt_uname(void);
void settings_save_wifi_ssid(void);
void settings_save_wifi_password(void);
void settings_save_wpm(void);

#endif
