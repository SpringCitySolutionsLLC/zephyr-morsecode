#!/usr/bin/env bash
#
# scripts/configure.sh
#
# Copyright 2021-2022 Spring City Solutions LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Remember doing this in Video 203?
# sudo apt-get install golang-go
# go get github.com/apache/mynewt-mcumgr-cli/mcumgr
# ~/go/bin/mcumgr --help
#
# This is not a complete script, but is more "inspiration" for 
# scripted API provisioning of new devices over a serial port.
# This would be quite handy for a wifi connected board which is
# not network accessible until after the wifi is configured so 
# configuring wifi access settings over wifi would not be terribly
# practical.
# To run this configure script "for real" you would have to change
# network-config.sh to have a connection string for your serial port.
# You would probably copy this "read only" version to a file named something like:
# configure-f767zi.sh
# and then edit the file configure-f767zi.sh to have the correct settings and 
# uncomment all the writing commands
#
source ~/zephyrproject/morsecode/scripts/network-config.sh
#
date
#
$MCUMGR $FULLCONNSTRING echo echo-test-ok
#
# Report uptime using the shell command, just to prove this works
$MCUMGR $FULLCONNSTRING shell exec uptime
#
# Network Settings
#
# Read/Set WiFi SSID
$MCUMGR $FULLCONNSTRING shell exec wifi_ssid
#$MCUMGR $FULLCONNSTRING shell exec wifi_ssid MyNetworkName
#
# Read/Set WiFi Password
$MCUMGR $FULLCONNSTRING shell exec wifi_password
#$MCUMGR $FULLCONNSTRING shell exec wifi_password MyNetworkPassword
#
# Read/Set MAC byte 1
$MCUMGR $FULLCONNSTRING shell exec mac_1
#$MCUMGR $FULLCONNSTRING shell exec mac_1 02
#
# Read/Set MAC byte 2
$MCUMGR $FULLCONNSTRING shell exec mac_2
#$MCUMGR $FULLCONNSTRING shell exec mac_2 80
#
# Read/Set MAC byte 3
$MCUMGR $FULLCONNSTRING shell exec mac_3
#$MCUMGR $FULLCONNSTRING shell exec mac_3 E1
#
# Read/Set MAC byte 4
$MCUMGR $FULLCONNSTRING shell exec mac_4
#$MCUMGR $FULLCONNSTRING shell exec mac_4 73
#
# Read/Set MAC byte 5
$MCUMGR $FULLCONNSTRING shell exec mac_5
#$MCUMGR $FULLCONNSTRING shell exec mac_5 83
#
# Read/Set MAC byte 6
$MCUMGR $FULLCONNSTRING shell exec mac_6
#$MCUMGR $FULLCONNSTRING shell exec mac_6 43
#
# Device Name Settings
#
# Read/Set hostname
$MCUMGR $FULLCONNSTRING shell exec hostname
#$MCUMGR $FULLCONNSTRING shell exec hostname f767zi
#
# Read/Set Kapua Displayname probably similar to or inspired by your hostname
$MCUMGR $FULLCONNSTRING shell exec kapua_displayname
#$MCUMGR $FULLCONNSTRING shell exec kapua_displayname Nucleo-F767zi
#
# Read/Set MQTT Client ID probably should match your hostname
$MCUMGR $FULLCONNSTRING shell exec mqtt_clientid
#$MCUMGR $FULLCONNSTRING shell exec mqtt_clientid f767zi
#
# Read/Set MQTT Username probably should match your hostname to minimized confusion
$MCUMGR $FULLCONNSTRING shell exec mqtt_uname
#$MCUMGR $FULLCONNSTRING shell exec mqtt_uname f767zi
#
# Kapua/MQTT settings
#
# Read/Set MQTT address aka your Kapua server IP address
$MCUMGR $FULLCONNSTRING shell exec mqtt_address
#$MCUMGR $FULLCONNSTRING shell exec mqtt_address 10.10.21.38
#
# Read/Set Kapua Appname
$MCUMGR $FULLCONNSTRING shell exec kapua_appname
#$MCUMGR $FULLCONNSTRING shell exec kapua_appname morsecode
#
# Read/Set Kapua Tenant aka Account
$MCUMGR $FULLCONNSTRING shell exec kapua_tenant
#$MCUMGR $FULLCONNSTRING shell exec kapua_tenant scs
#
# Read/Set MQTT Password
$MCUMGR $FULLCONNSTRING shell exec mqtt_pword
#$MCUMGR $FULLCONNSTRING shell exec mqtt_pword Password1234567890+
#
# Application Settings
#
# Read/Set WPM aka the speed at which the Morse Code message is transmitted
$MCUMGR $FULLCONNSTRING shell exec wpm
#$MCUMGR $FULLCONNSTRING shell exec wpm 18
#
exit 0
