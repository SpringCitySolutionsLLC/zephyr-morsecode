#!/usr/bin/env python3
#
# scripts/kapua-config/tenant-scs.py
#
# This set of scripts automatically configure a default Docker install of Kapua.
# The passwords set by these scripts are insecure defaults you should change.
# These scripts would require considerable personalization and editing but are a good start.
#
# Copyright 2021-2022 Spring City Solutions LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# These are standard system libraries
import json
import pprint
pp = pprint.PrettyPrinter(indent=4)

# Should have installed Python library this back in Video 203, but if not...
# python -m pip3 install --user requests
import requests
from requests.auth import HTTPBasicAuth

# Your Kapua base URL is almost certainly somewhat different.
api_url = "http://10.10.72.5:8081/v1"

# These are the default API account for a Docker Kapua install.
# You probably would want to change these if 
# the server were on the internet...
api_uname = "kapua-sys"
api_pword = "kapua-password"

print("Obtaining authentication token")
# Create request json
authentication_json = { "username" : api_uname, "password" : api_pword }
# pp.pprint(authentication_json)
# Get authentication token
# Yeah I know its intuitive that "getting" an authentication token would be a HTTP GET
# but its actually a HTTP POST lol
response = requests.post(api_url + "/authentication/user", json = authentication_json )
responsedict = json.loads(response.text)
# pp.pprint(responsedict)
tokenId = responsedict["tokenId"]
headers = { 'Authorization' : 'Bearer ' + tokenId, 'Accept' : 'application/json' }
# print(headers)

print("Create tenant account")
# Create request json
account_json = {
    "name": "scs",
    "organizationName": "Spring City Solutions LLC",
    "organizationEmail": "support@springcitysolutions.com",
    "organizationPersonName": "Spring City Solutions LLC"
}
# pp.pprint(account_json)
response = requests.post(api_url + "/_/accounts", json=account_json, headers=headers)
responsedict = json.loads(response.text)
# pp.pprint(responsedict)
scopeId = responsedict["id"]
print("Scope ID is:", scopeId)

print("Configure UserService for tenant account")
# Create request json
userservice_json = {
  "id": "org.eclipse.kapua.service.user.UserService",
  "properties": {
    "property": [
      {
        "name": "maxNumberChildEntities",
        "type": "Integer",
        "value": [
          "0"
        ]
      },
      {
        "name": "infiniteChildEntities",
        "type": "Boolean",
        "value": [
          "true"
        ]
      }
    ]
  }
}
# pp.pprint(userservice_json)
response = requests.put(api_url + "/" + scopeId + "/serviceConfigurations/org.eclipse.kapua.service.user.UserService", json=userservice_json, headers=headers)
print("UserService Response : ", response)

print("Configure CredentialService for tenant account")
# Create request json
credentialservice_json = {
  "id": "org.eclipse.kapua.service.authentication.credential.CredentialService",
  "properties": {
    "property": [
      {
        "name": "lockoutPolicy.enabled",
        "type": "Boolean",
        "value": [
          "false"
        ]
      },
      {
        "name": "lockoutPolicy.resetAfter",
        "type": "Integer",
        "value": [
          "60"
        ]
      },
            {
        "name": "lockoutPolicy.maxFailures",
        "type": "Integer",
        "value": [
          "60"
        ]
      },
      {
        "name": "lockoutPolicy.lockDuration",
        "type": "Integer",
        "value": [
          "60"
        ]
      },
    ]
  }
}
# pp.pprint(credentialservice_json)
response = requests.put(api_url + "/" + scopeId + "/serviceConfigurations/org.eclipse.kapua.service.authentication.credential.CredentialService", json=credentialservice_json, headers=headers)
print("CredentialService Response : ", response)

print("Configure DeviceRegistryService for tenant account")
# Create request json
deviceregistryservice_json = {
  "id": "org.eclipse.kapua.service.device.registry.DeviceRegistryService",
  "properties": {
    "property": [
      {
        "name": "infiniteChildEntities",
        "type": "Boolean",
        "value": [
          "true"
        ]
      },      
      {
        "name": "maxNumberChildEntities",
        "type": "Integer",
        "value": [
          "0"
        ]
      },
    ]
  }
}
# pp.pprint(deviceregistryservice_json)
response = requests.put(api_url + "/" + scopeId + "/serviceConfigurations/org.eclipse.kapua.service.device.registry.DeviceRegistryService", json=deviceregistryservice_json, headers=headers)
print("DeviceRegistryService Response : ", response)

print("Configure GroupService for tenant account")
# Create request json
groupservice_json = {
  "id": "org.eclipse.kapua.service.authorization.group.GroupService",
  "properties": {
    "property": [
      {
        "name": "infiniteChildEntities",
        "type": "Boolean",
        "value": [
          "true"
        ]
      },
      {
        "name": "maxNumberChildEntities",
        "type": "Integer",
        "value": [
          "0"
        ]
      },
    ]
  }
}
# pp.pprint(groupservice_json)
response = requests.put(api_url + "/" + scopeId + "/serviceConfigurations/org.eclipse.kapua.service.authorization.group.GroupService", json=groupservice_json, headers=headers)
print("GroupService Response : ", response)

print("Configure JobService for tenant account")
# Create request json
jobservice_json = {
  "id": "org.eclipse.kapua.service.job.JobService",
  "properties": {
    "property": [
      {
        "name": "infiniteChildEntities",
        "type": "Boolean",
        "value": [
          "true"
        ]
      },
      {
        "name": "maxNumberChildEntities",
        "type": "Integer",
        "value": [
          "0"
        ]
      },
    ]
  }
}
# pp.pprint(jobservice_json)
response = requests.put(api_url + "/" + scopeId + "/serviceConfigurations/org.eclipse.kapua.service.job.JobService", json=jobservice_json, headers=headers)
print("JobService Response : ", response)

print("Configure MessageStoreService for tenant account")
# Create request json
messagestoreservice_json = {
  "id": "org.eclipse.kapua.service.datastore.MessageStoreService",
  "properties": {
    "property": [
      {
        "name": "dataIndexBy",
        "type": "String",
        "value": [
          "SERVER_TIMESTAMP"
        ]
      },
      {
        "name": "dataTTL",
        "type": "Integer",
        "value": [
          "1"
        ]
      },
      {
        "name": "rxByteLimit",
        "type": "Integer",
        "value": [
          "0"
        ]
      },
      {
        "name": "enabled",
        "type": "Boolean",
        "value": [
          "true"
        ]
      },
    ]
  }
}
# pp.pprint(messagestoreservice_json)
response = requests.put(api_url + "/" + scopeId + "/serviceConfigurations/org.eclipse.kapua.service.datastore.MessageStoreService", json=messagestoreservice_json, headers=headers)
print("MessageStoreService Response : ", response)

print("Configure TagService for tenant account")
# Create request json
tagservice_json = {
  "id": "org.eclipse.kapua.service.tag.TagService",
  "properties": {
    "property": [
      {
        "name": "infiniteChildEntities",
        "type": "Boolean",
        "value": [
          "true"
        ]
      },
      {
        "name": "maxNumberChildEntities",
        "type": "Integer",
        "value": [
          "0"
        ]
      },
    ]
  }
}
# pp.pprint(tagservice_json)
response = requests.put(api_url + "/" + scopeId + "/serviceConfigurations/org.eclipse.kapua.service.tag.TagService", json=tagservice_json, headers=headers)
print("TagService Response : ", response)

quit()

response = requests.get(api_url + "/" + scopeId + "/serviceConfigurations", headers=headers)
responsedict = json.loads(response.text)
pp.pprint(responsedict)
