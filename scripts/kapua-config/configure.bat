:: scripts/kapua-config/configure.bat
::
:: This set of scripts automatically configure a default Docker install of Kapua.
:: The passwords set by these scripts are insecure defaults you should change.
:: These scripts would require considerable personalization and editing but are a good start.
::
:: Copyright 2021-2022 Spring City Solutions LLC
::
:: Licensed under the Apache License, Version 2.0 (the "License");
:: you may not use this file except in compliance with the License.
:: You may obtain a copy of the License at
::
:: http://www.apache.org/licenses/LICENSE-2.0
::
:: Unless required by applicable law or agreed to in writing, software
:: distributed under the License is distributed on an "AS IS" BASIS,
:: WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
:: See the License for the specific language governing permissions and
:: limitations under the License.

:: First you create the tenant
python ./tenant-scs.py

:: Then you create the roles for the tenant
python ./role-admin.py
python ./role-user.py
python ./role-device.py
python ./role-bridge.py

:: Then you create the users, connected to the roles, for the tenant
python ./user-vince.py
python ./user-dockerkura.py
python ./user-f767zi.py
python ./user-mqttrouter.py
python ./user-pikura.py
python ./user-testuser.py

exit