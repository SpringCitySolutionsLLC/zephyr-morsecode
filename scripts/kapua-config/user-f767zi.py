#!/usr/bin/env python3
#
# scripts/kapua-config/user-f767zi.py
#
# This set of scripts automatically configure a default Docker install of Kapua.
# The passwords set by these scripts are insecure defaults you should change.
# These scripts would require considerable personalization and editing but are a good start.
#
# Copyright 2021-2022 Spring City Solutions LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# These are standard system libraries
import json
import pprint
pp = pprint.PrettyPrinter(indent=4)

# Should have installed Python library this back in Video 203, but if not...
# python -m pip3 install --user requests
import requests
from requests.auth import HTTPBasicAuth

# Your Kapua base URL is almost certainly somewhat different.
api_url = "http://10.10.72.5:8081/v1"

# These are the default API account for a Docker Kapua install.
# You probably would want to change these if 
# the server were on the internet...
api_uname = "kapua-sys"
api_pword = "kapua-password"

print("Obtaining authentication token")
# Create request json
authentication_json = { "username" : api_uname, "password" : api_pword }
# pp.pprint(authentication_json)
# Get authentication token
# Yeah I know its intuitive that "getting" an authentication token would be a HTTP GET
# but its actually a HTTP POST lol
response = requests.post(api_url + "/authentication/user", json = authentication_json )
responsedict = json.loads(response.text)
# pp.pprint(responsedict)
tokenId = responsedict["tokenId"]
headers = { 'Authorization' : 'Bearer ' + tokenId, 'Accept' : 'application/json' }
# print(headers)

print("Obtaining scopeId")
# Create request json
accounts_json = { "name" : "scs" }
# pp.pprint(authentication_json)
response = requests.get(api_url + "/_/accounts", json = accounts_json, headers=headers)
responsedict = json.loads(response.text)
#pp.pprint(responsedict)
scopeId = responsedict["items"][0]["id"]
print("Scope ID is:", scopeId)

print("Obtaining roleId")
response = requests.get(api_url + "/" + scopeId + "/roles", headers=headers)
responsedict = json.loads(response.text)
# pp.pprint(responsedict)
# This nonsense because Swagger docs claims roles_json = { "name": "Admin" } works.
# ... but it don't work.
for attrs in responsedict['items']:
    if attrs['name'] == "Device":
        roleId = attrs['id']
        break
else:
    print('Nothing found!')
print("Role ID is:", roleId)

print("Create user account")
# Create request json
account_json = {
    "name": "f767zi",
    "displayName": "STM32 Nucleo-F767zi Dev Board"
}
# pp.pprint(account_json)
response = requests.post(api_url + "/" + scopeId + "/users", json=account_json, headers=headers)
responsedict = json.loads(response.text)
#pp.pprint(responsedict)
userId = responsedict["id"]
print("User ID is:", userId)

print("Assign a credential aka password")
# Create request json
credential_json = {
    "userId": userId,
    "credentialType": "PASSWORD",
    "credentialStatus": "ENABLED",
    "credentialKey": "Password1234567890+",
}
# pp.pprint(credential_json)
response = requests.post(api_url + "/" + scopeId + "/credentials", json=credential_json, headers=headers)
responsedict = json.loads(response.text)
# pp.pprint(responsedict)

print("Assign a role")
# Create request json
accessinfo_json = {
    "userId": userId,
    "roleIds": [
        roleId
    ]
}
# pp.pprint(accessinfo_json)
response = requests.post(api_url + "/" + scopeId + "/accessinfos", json=accessinfo_json, headers=headers)
responsedict = json.loads(response.text)
pp.pprint(responsedict)
