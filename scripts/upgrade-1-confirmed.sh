#!/usr/bin/env bash
#
# scripts/upgrade-1-confirmed.sh
#
# Copyright 2021-2022 Spring City Solutions LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Remember doing this in Video 203?
# sudo apt-get install golang-go
# go get github.com/apache/mynewt-mcumgr-cli/mcumgr
# ~/go/bin/mcumgr --help
#
source ~/zephyrproject/morsecode/scripts/network-config.sh
#
date
#
$MCUMGR $FULLCONNSTRING echo echo-test-ok
#
echo uploading new firmware to slot 1
$MCUMGR $FULLCONNSTRING image upload ~/zephyrproject/morsecode/build/zephyr/zephyr.signed.confirmed.bin
#
echo image list after upload
$MCUMGR $FULLCONNSTRING image list
#
echo Next, run ./upgrade-2.sh and cut and paste the hash value from slot 1 here, assuming it was a good upload
#
exit 0
