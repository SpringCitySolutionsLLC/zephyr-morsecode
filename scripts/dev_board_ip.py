#!/usr/bin/env python3
#
# dev_board_ip.py
#
# Copyright 2021-2022 Spring City Solutions LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# These are standard system libraries
import json
import pprint
pp = pprint.PrettyPrinter(indent=4)

# Should have installed Python library this back in Video 203, but if not...
# python -m pip3 install --user requests
import requests
from requests.auth import HTTPBasicAuth

# Your hawkBit IP Address is almost certainly different.
api_url = "http://10.10.20.13:8080/rest/v1"

# These are the default accounts for a Docker hawkBit install.
# You probably would want to change these if 
# the server were on the internet...
api_uname = "admin"
api_pword = "admin"

# Your individual STM32 chip unique ID will be different 
# than my chip's ID, yet appear similar.
board_name = "nucleo_f767zi-30373738343851190034003c"

# API Documented at https://www.eclipse.org/hawkbit/apis/mgmt/targets/
response = requests.get(api_url + "/targets/" + board_name, auth = HTTPBasicAuth(api_uname, api_pword));
responsedict = json.loads(response.text)
# pp.pprint(responsedict)

# We will use this script inside shell backticks 
# to magically insert the IP address in existing 
# scripts so no newline allowed.
print(responsedict["ipAddress"], end='')
