#!/usr/bin/env python3
#
# scripts/hawkbit-upload.py
#
# Copyright 2021-2022 Spring City Solutions LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import datetime

# Should have installed Python library this back in Video 203, but if not...
# python -m pip3 install --user requests
import requests
from requests.auth import HTTPBasicAuth

import json

import pprint
pp = pprint.PrettyPrinter(indent=4)

# Yours will be different
api_url = "http://10.10.72.2:8080/rest/v1"

# Yours will be different, but similar.
upload_path = "C:/Users/vince/zephyrproject/morsecode/build/zephyr/zephyr.signed.bin"

# Yours will be different
vendor = "Spring City Solutions LLC"

# I use the same name for software module and distribution.
# I imagine, you could in theory use distribution names with
# suffixes like -test or -dev or -production and separate 
# upload scripts or as an option or rename or something.
distribution_name = "morsecode"
module_name = "morsecode"

# These are the default Docker deployment API credentials
api_uname = "admin"
api_pword = "admin"

description = "Uploaded by upload.py script"

now = datetime.now()

version = ""
version += '%04d' % now.year
version += '-'
version += '%02d' % now.month
version += '-'
version += '%02d' % now.day
version += '.'
version += '%02d' % now.hour
version += ':'
version += '%02d' % now.minute
version += ':'
version += '%02d' % now.second

# This API is documented at https://www.eclipse.org/hawkbit/apis/mgmt/softwaremodules/
print("Creating new software module for version", version)
# API will fail if you pass one item.
# API will work if you pass a list containing one item.
# I never considered someone trying to create a list of modules at once so 
# this was super annoying.
software_module_json = [ {"name" : module_name, "type" : "application", "version" : version, "vendor" : vendor, "description" : description } ]
response = requests.post(api_url + "/softwaremodules", json=software_module_json, auth=HTTPBasicAuth(api_uname, api_pword));
responsedict = json.loads(response.text)
# pp.pprint(responsedict)
module_id = responsedict[0].get('id')
print("New software module ID number is", module_id)

# This API is documented at https://www.eclipse.org/hawkbit/apis/mgmt/softwaremodules/
print("Uploading artifact into software module")
artifact_file = open(upload_path, "rb")
artifact_json = [ {"filename" : "zephyr.signed.bin" } ]
response = requests.post(api_url + "/softwaremodules/" + str(module_id) + "/artifacts", json=artifact_json, files={"file": artifact_file}, auth=HTTPBasicAuth(api_uname, api_pword));
reponsedict = json.loads(response.text)
# pp.pprint(responsedict)
# Online docs claim this will return a size successfully uploaded.  It does not.

# This API is documented at https://www.eclipse.org/hawkbit/apis/mgmt/distributionsets/
print("Creating a new distribution")
# Another strange "must be a list" situation.
# Good luck changing the "type", that comes from distributionsettypes and is actually named "key"
distribution_json = [ { "name" : module_name, "description" : description, "version" : version, "requireMigrationStep" : "false", "type" : "app", "modules" : [ { "id" : module_id  } ] } ]
response = requests.post(api_url + "/distributionsets/", json=distribution_json, auth=HTTPBasicAuth(api_uname, api_pword));
responsedict = json.loads(response.text)
# pp.pprint(responsedict)
