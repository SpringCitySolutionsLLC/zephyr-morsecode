#!/usr/bin/env python3
#
# scripts/kapua-read.py
#
# Pull some metric data from one device.
# Not terribly useful, but its a great place to start.
#
# Copyright 2021-2022 Spring City Solutions LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# These are standard system libraries
import json
import pprint
pp = pprint.PrettyPrinter(indent=4)

# Should have installed Python library this back in Video 203, but if not...
# python -m pip3 install --user requests
import requests
from requests.auth import HTTPBasicAuth

# Your Kapua base URL is almost certainly somewhat different.
api_url = "http://10.10.21.38:8081/v1"

# These are the default API account for a Docker Kapua install.
# You probably would want to change these if 
# the server were on the internet...
api_uname = "kapua-sys"
api_pword = "kapua-password"

print("Obtaining authentication token")
# Create request json
authentication_json = { "username" : api_uname, "password" : api_pword }
# pp.pprint(authentication_json)
# Get authentication token
# Yeah I know its intuitive that "getting" an authentication token would be a HTTP GET
# but its actually a HTTP POST lol
response = requests.post(api_url + "/authentication/user", json = authentication_json )
responsedict = json.loads(response.text)
# pp.pprint(responsedict)
tokenId = responsedict["tokenId"]
headers = { 'Authorization' : 'Bearer ' + tokenId, 'Accept' : 'application/json' }
# print(headers)

print("Obtaining scopeId")
# Create request json
accounts_json = { "name" : "scs" }
# pp.pprint(authentication_json)
response = requests.get(api_url + "/_/accounts", json = accounts_json, headers=headers)
responsedict = json.loads(response.text)
#pp.pprint(responsedict)
scopeId = responsedict["items"][0]["id"]
print("Scope ID is:", scopeId)

print("Obtaining a metric ID for transmissions")
# Create request json
# Supposed to be able to filter.  Does not work.
metrics_json = { "scopeId" : scopeId, "clientId" : "f767zi", "channel" : "morsecode", "name" : "transmissions" }
# pp.pprint(authentication_json)
response = requests.get(api_url + "/" + scopeId + "/data/metrics", json = metrics_json, headers=headers)
responsedict = json.loads(response.text)
# pp.pprint(responsedict)
#scopeId = responsedict["items"][0]["id"]
#print("Scope ID is:", scopeId)
for attrs in responsedict['items']:
    if attrs['name'] == "transmissions":
        metricId = attrs['id']
        break
else:
    print('Nothing found!')
print("Metric ID is:", metricId)

print("Obtaining the lastMessageId for that metric ID")
# Create request json
metrics_json = { "scopeId" : scopeId, "metricInfoId" : metricId }
# pp.pprint(authentication_json)
response = requests.get(api_url + "/" + scopeId + "/data/metrics/" + metricId, json = metrics_json, headers=headers)
responsedict = json.loads(response.text)
#pp.pprint(responsedict)
lastMessageId = responsedict['lastMessageId']
print("lastMessageId is:", lastMessageId)

print("Obtaining the data related to the lastMessageId")
# Create request json
message_json = { "scopeId" : scopeId, "datastoreMessageId" : lastMessageId }
# pp.pprint(authentication_json)
response = requests.get(api_url + "/" + scopeId + "/data/messages/" + lastMessageId, json = message_json, headers=headers)
responsedict = json.loads(response.text)
pp.pprint(responsedict)


quit()

response = requests.get(api_url + "/" + scopeId + "/data/metrics", headers=headers)
responsedict = json.loads(response.text)
pp.pprint(responsedict)
