#!/usr/bin/env bash
#
# scripts/upgrade-2.sh
#
# Copyright 2021-2022 Spring City Solutions LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Remember doing this in Video 203?
# sudo apt-get install golang-go
# go get github.com/apache/mynewt-mcumgr-cli/mcumgr
# ~/go/bin/mcumgr --help
#
source ~/zephyrproject/morsecode/scripts/network-config.sh
#
date
#
$MCUMGR $FULLCONNSTRING echo echo-test-ok
#
echo Enabling a one time test of booting the image identified by hash value
$MCUMGR $FULLCONNSTRING image test $1 
#
echo And we reset the dev board.
$MCUMGR $FULLCONNSTRING reset
#
echo Next, very carefully test the new firmware, and if its any good, make it permanent using -3.
echo or if its junk the mcuboot will revert on the next reset and you restart with script -1.
echo ./network-upgrade-3.sh
#
exit 0
