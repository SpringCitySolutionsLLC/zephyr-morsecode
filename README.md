# Zephyr morse code blinker

## Branch Video-301

Use this branch with the video #301 as seen at the following Youtube URL:

TODO: URL

Branch Video-301 demonstrates a simple morse code blinker.

It doesn't do much, but it does exercise the entire build process from editing in the IDE to running code on the board.

This branch is also used as a demonstration platform for developing on Win10 while compiling in the WSL and flash programming back on Win10 using STM32CubeProgrammer, demonstrating cmake and its CMakeLists.txt file, and experimenting with kconfig and west's menuconfig target and the prj.conf file.

## Branch Video-302

Use this branch with the video #302 as seen at the following Youtube URL:

TODO: URL

Branch Video-302 walks thru using source code management using git and gitlab, using the devicetree in zephyr, porting to a new board, and demonstrating the automated security hardening system in Zephyr.

The code itself is cleaned up a bit, the morse code part of the project is in separate files (Zephyr has no problem linking multiple files) and blinks out a different morse code message.

## Branch Video-303

Use this branch with the video #303 as seen at the following Youtube URL:

TODO: URL

Branch Video-303 converts from running Zephyr on "raw hardware" as the boot process, to using MCUBoot as the crypto secured boot process, converting the morse code blinker software to run as something booted from MCUBoot rather than booting the CPU, and using various utilities such as mcumgr and other DFU utilities to individually remotely upgrade and control the dev board over an ethernet connection.

The code itself blinks out yet another new morse code message.

## Branch Video-304

Use this branch with the video #304 as seen at the following Youtube URL:

TODO: URL

Branch Video-304 converts from manual firmware updates over ethernet to using a hawkBit server on my internal LAN.

The code itself blinks out yet another new morse code message.

## Branch Video-305

Use this branch with the video #305 as seen at the following Youtube URL:

TODO: URL

Branch Video-305 adds support for the settings subsystem.  The settings include MQTT server hostname, MQTT account and password, etc.  Those settings require shell commands to view and set them.  Those settings are used by the newly added MQTT server connection.  Now the device can report plain text telemetry back to the MQTT server.  After that, Eclipse Kapua protobuf support was added such that the Kapua device management system can read, store, and report telemetry data.

The code itself blinks out a new morse code message for this branch.

## Branch Video-306

Use this branch with the video #306 as seen at the following Youtube URL:

TODO: URL

Branch Video-306 adds automation for hawkBit and Kapua.

The code itself blinks out a new morse code message for this branch.

## License Copyright etc

Copyright 2021-2022 Spring City Solutions LLC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Icon made from http://www.onlinewebfonts.com/icon aka Icon Fonts is licensed by CC BY 3.0

src/kurapayload.proto was downloaded for interoperability purposes from:
https://github.com/eclipse/kura/blob/develop/kura/org.eclipse.kura.core.cloud/src/main/protobuf/kurapayload.proto
and is used under the "Eclipse Public License 2.0" as seen at:
https://github.com/eclipse/kura/blob/develop/LICENSE

